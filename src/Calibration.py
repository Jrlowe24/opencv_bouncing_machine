import numpy as np
import cv2
import sys


class Calibration:

    def __init__(self):
        # termination criteria
        criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 30, 0.001)

        # prepare object points, like (0,0,0), (1,0,0), (2,0,0) ....,(6,5,0)
        objp = np.zeros((6*9,3), np.float32)
        objp[:,:2] = np.mgrid[0:9,0:6].T.reshape(-1,2)

        # Arrays to store object points and image points from all the images.
        objpoints = [] # 3d point in real world space
        imgpoints = [] # 2d points in image plane.

        # images = glob.glob('../media/callibration/*.jpg')
        cap = cv2.VideoCapture(1)
        cap.set(3, 640)
        cap.set(4, 480)
        cap.set(5, 30)
        frame_num = 0

        while cap.isOpened():
            ret, img = cap.read()
            frame_num = frame_num + 1
            if ret is False:
                print('end of stream')
                break

            if frame_num > 80:
                frame_num = 0
                gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
                # img1 = cv2.resize(img, (640, 360), interpolation=cv2.INTER_AREA)
                cv2.imshow('img', img)
                # Find the chess board corners
                ret, corners = cv2.findChessboardCorners(gray, (9,6),None)
                # If found, add object points, image points (after refining them)
                if ret == True:
                    objpoints.append(objp)

                    corners2 = cv2.cornerSubPix(gray,corners,(11,11),(-1,-1),criteria)
                    imgpoints.append(corners2)

                    # Draw and display the corners
                    img = cv2.drawChessboardCorners(img, (9,6), corners2,ret)
                    img2 = img
                    # img2 = cv2.resize(img2, (640, 360), interpolation=cv2.INTER_AREA)
                    cv2.imshow('img', img2)


            if cv2.waitKey(1) == ord('q'):
                break

        ret, mtx, dist, rvecs, tvecs = cv2.calibrateCamera(objpoints, imgpoints, (640, 480), None, None)
        # print(ret)
        print('Camera Matrix: ', mtx)
        print('Distortion Matrix: ', dist)

        cv2.destroyAllWindows()


class TestPoints:

    def __init__(self):
        cap = cv2.VideoCapture(1)
        cap.set(3, 1280)
        cap.set(4, 720)
        cap.set(5, 120)
        self.mtx_big = np.array([[722.46847303, 0.0, 640], [0.0, 721.60561939, 360], [0.0, 0.0, 1.0]])
        self.mtx = np.array([[722.46847303/2, 0.0, 640/2], [0.0, 721.60561939/2, 360/2], [0.0, 0.0, 1.0]])
        cv2.namedWindow('image')
        cv2.setMouseCallback('image', self.mousePosition)
        self.mouse_x = 0
        self.mouse_y = 0

        while cap.isOpened():
            ret, frame = cap.read()
            frame = self.undistort(frame)
            if not ret:
                print('Camera Disconnected')
                break

            output = cv2.resize(frame, (640, 360), interpolation=cv2.INTER_AREA)
            # output = self.drawPoints(output)

            u = self.mouse_x
            v = self.mouse_y - 10
            z = 304-11.5

            x = int((u - self.mtx[0][2]) * z / self.mtx[0][0])
            y = int((v - self.mtx[1][2]) * z / self.mtx[1][1])

            print(x * 2, y * 2)

            output = cv2.circle(output, (u, v), 2, (0, 0, 255), -1)
            output = cv2.circle(output, (320, 180), 2, (0, 255, 0), -1)

            cv2.imshow('image', output)

            if cv2.waitKey(1) == ord('q'):
                break

    def mousePosition(self, event, x, y, flags, param):
        if event == cv2.EVENT_MOUSEMOVE:
            self.mouse_x = x
            self.mouse_y = y

    def undistort(self, image):

        dist = np.array([-3.54264337e-01, 1.86872938e-01, 1.14042263e-03, 3.30026432e-04, -6.06989160e-02])

        newcameramtx, _ = cv2.getOptimalNewCameraMatrix(self.mtx_big, dist, (640, 360), 0.5)
        # undistort
        return cv2.undistort(image, self.mtx_big, dist, None, newcameramtx)


if __name__ == "__main__":
    if sys.argv[1] == "test":
        TestPoints()
    elif sys.argv[1] == "calibrate":
        Calibration()

import math
import vpython


class IKGui:

    def __init__(self):
        self.surface = vpython.box(pos=vpython.vector(0,0,0), length=150, width=150, height=5, up=vpython.vector(0,0,1))
        vpython.scene.camera.pos = vpython.vector(0,0, 300)
        # vpython.scene.camera.axis = vpython.vector
        vpython.scene.width = 500
        vpython.scene.height = 500
        vpython.scene.userzoom = 1
        vpython.scene.userspin = 1
        vpython.scene.autoscale = False
        pass

    def update(self, normal, center_z):
        a,b,c = normal
        self.surface.up = vpython.vector(a,-b,c)
        self.surface.pos.z = center_z


class InverseKinematics:

    def __init__(self, arm_length_1, arm_length_2, plane, show_gui):
        self.arm_length_1 = arm_length_1
        self.arm_length_2 = arm_length_2
        self.plane = plane
        self.min_angle = math.radians(-14)
        self.max_angle = math.radians(60)
        self.center_z = 0  # height of the middle point on the plane
        self.z1 = 0
        self.z2 = 0
        self.z3 = 0
        self.offset1 = 0
        self.offset2 = 10
        self.offset3 = 8
        self.offset_angle1 = round(math.asin(self.offset1 / self.arm_length_1), 2)
        self.offset_angle2 = round(math.asin(self.offset2 / self.arm_length_1), 2)
        self.offset_angle3 = round(math.asin(self.offset3 / self.arm_length_1), 2)
        self.angle1 = 0  # servo angle starting from min angle
        self.angle2 = 0
        self.angle3 = 0
        self.set = 0,0,0
        self.normal = (0, 0, 1)
        self.show_gui = show_gui
        if show_gui:
            self.gui = IKGui()

    #  Convert plane equation into 3 servo angles
    def update(self, normal):
        self.normal = normal
        self.calc_z()
        self.set_angles()

        # print(' z1: ', self.z1 - self.offset1, ' z2: ', self.z2 - self.offset2, ' z3: ', self.z3 - self.offset3, ' center: ', self.center_z)
        # print(' angle1: ', self.angle1 - self.offset_angle1, ' angle2: ', self.angle2 - self.offset_angle2, ' angle3: ', self.angle3 - self.offset_angle3)

        if self.show_gui:
            self.gui.update(self.normal, self.center_z)

    def raise_plate(self, amount):
        self.center_z += amount
        self.calc_z()
        if max([self.z1,self.z2, self.z3]) >= self.arm_length_1:
            print('Error: Plate Height Unreachable')

    # calculates the vertical offsets for the 3 points on the plane
    def calc_z(self):
        A,B,C = self.normal
        point1 = self.plane[1]
        point2 = self.plane[2]
        point3 = self.plane[3]
        D = C * self.center_z
        self.z1 = round((1 / C) * (D - A * point1[0] - B * point1[1]), 2) + self.offset1
        self.z2 = round((1 / C) * (D - A * point2[0] - B * point2[1]), 2) + self.offset2
        self.z3 = round((1 / C) * (D - A * point3[0] - B * point3[1]), 2) + self.offset3

        lowest_point = min([self.z1, self.z2, self.z3])
        if lowest_point < 0:
            self.raise_plate(-lowest_point)

    # sets angle1 - angle3 and handles the limits
    def set_angles(self):

        self.angle1 = self.angle_calc(self.z1)
        self.angle2 = self.angle_calc(self.z2)
        self.angle3 = self.angle_calc(self.z3)

        if self.angle1 > self.max_angle:
            self.angle1 = self.max_angle
        if self.angle2 > self.max_angle:
            self.angle2 = self.max_angle
        if self.angle3 > self.max_angle:
            self.angle3 = self.max_angle

        if self.angle1 < self.offset_angle1:
            self.angle1 = self.offset_angle1
        if self.angle2 < self.offset_angle2:
            self.angle2 = self.offset_angle2
        if self.angle3 < self.offset_angle3:
            self.angle3 = self.offset_angle3

        self.angle1 = round(math.degrees(self.angle1), 0)
        self.angle2 = round(math.degrees(self.angle2), 0)
        self.angle3 = round(math.degrees(self.angle3), 0)

        self.set = self.angle1, self.angle3, self.angle3

    # calculates the angle based on the z position
    def angle_calc(self, z):
        length_under = self.arm_length_1 * math.sin(-self.min_angle)
        if z < length_under:
            y = length_under - z
            angle = (-self.min_angle) - math.asin(y / self.arm_length_1)
        else:
            y = z - length_under
            try:
                angle = (-self.min_angle) + math.asin(y / self.arm_length_1)
            except ValueError:
                print('Plane Out of Bounds')
                angle = self.max_angle

        return angle

    def bounce(self):
        self.center_z = self.center_z + 20


# center, bottom left, top, bottom right
points = (0, 0, 0), (-101.035, -87.5, 0), (0, 87.5, 0), (101.035, -87.5, 0)
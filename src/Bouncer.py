from IK import InverseKinematics as IK
from SerialConnect import SerialConnect
import numpy as np
import time


class BounceController:

    def __init__(self):
        plane = (0, 0, 0), (-101.035, -87.5, 0), (0, 87.5, 0), (101.035, -87.5, 0)
        self.IK = IK(58, 60, plane, False)
        self.connection = SerialConnect('/dev/cu.wchusbserial1420', 115200)
        self.time_since_last_bounce = 0
        self.bouncing = False
        self.num_frames_to_predict_bounce = 50
        self.bounce_loc = (0, 0)
        self.fps = 30
        self.time_per_frame = 1 / self.fps
        self.rolling = False  # True if ball stays at surface level for longer than a "bounce"
        self.path = []
        self.frame_times = []
        self.next_bounce_time = 0
        self.has_bounced = False
        self.bounce_times = []

    def update(self, path, frame_times):
        self.path = path
        self.frame_times = frame_times
        x, y, z = path[-1] # current ball coordinates
        relative_z = 282 - z

        # assume 0.00834 seconds per frame
        if len(self.bounce_times) == 2:
            last_air_time = self.bounce_times[1] - self.bounce_times[0]
            num_frames_in_last_bounce = int(1 / 120 * last_air_time)
            # a little more than half the bounce time to calculate the new bounce
            self.num_frames_to_predict_bounce = (num_frames_in_last_bounce / 2) + 10

        if self.time_since_last_bounce > self.num_frames_to_predict_bounce:
            # if 50 frames have passed since it bounced, predict the next bounce time and location
            self.next_bounce_time = self.time_till_next_bounce(method="symmetry")
            # self.bounce_loc = self.predict_bounce_loc()

        if not self.has_bounced and relative_z < 10:  # the ball was dropped for the first time
            self.has_bounced = True
            self.time_since_last_bounce = 0
            self.bounce_times.append(time.time())
            print('first bounce')

        if not self.bouncing and self.next_bounce_time - time.time() < 0.05:  # "head start" time for bounce
            self.bouncing = True
            self.time_since_last_bounce = 0
            self.IK.center_z += 15
            self.bounce_times.append(time.time())

        if self.bouncing and self.time_since_last_bounce > 15:  # reset surface after 15 frames
            self.IK.center_z -= 15
            self.bouncing = False

        self.IK.update((0, 0, 1))
        self.connection.send(self.IK.set)

        if self.has_bounced:
            self.time_since_last_bounce += 1
        if len(self.bounce_times) > 2:
            self.bounce_times.pop(0)

    def time_till_next_bounce(self, method="symmetry"):
        # find the time change between the last min and the last max.
        path = self.path[-self.num_frames_to_predict_bounce:]
        times = self.time_per_frame[-self.num_frames_to_predict_bounce:]
        z_points = [i[2] for i in path]
        z_points = [282 - point for point in z_points]  # relative to position above plate 0 position

        t1_index = path.index(min(z_points))
        t2_index = path.index(max(z_points))

        if method == "curve":  # second degree curve fit from first portion of flight
            times = np.cumsum(times)
            times = np.insert(times, 0, 0)
            curve = np.polyfit(times[t1_index:], z_points[t1_index:], 2)  # points from the previous min and on
            roots = np.roots(curve)
            time_since = sum(times[t1_index:])
            return max(roots) - time_since + time.time()

        elif method == "symmetry":  # assume time to hit peak is the same time to fall back down
            time_to_fall = sum(times[t1_index:t2_index])  # sum the time between frames
            time_since = sum(times[t2_index:])
            return time_to_fall - time_since + time.time()  # amount of time until next bounce

        elif method == "physics":  # physics based approach maybe?
            pass

    def predict_bounce_loc(self):
        path = self.path[-self.num_frames_to_predict_bounce:]  # first 50 frame locations after ball bounces
        times = self.frame_times[-self.num_frames_to_predict_bounce:]
        times = np.cumsum(times)
        times = np.insert(times, 0, 0)
        x_path = [i[0] for i in path]
        y_path = [i[1] for i in path]
        z_points = [i[2] for i in path]

        t1_index = path.index(min(z_points)) + 10

        vel_x = np.polyfit(times[t1_index:], x_path[t1_index:], 1)
        vel_y = np.polyfit(times[t1_index:], y_path[t1_index:], 1)

        delta_t = self.next_bounce_time - time.time()

        x = x_path[-1] + (vel_x * delta_t)
        y = y_path[-1] + (vel_y * delta_t)

        # ----------Calculate Normal-----------------------

        air_time = self.bounce_times[1] - self.bounce_times[0]

        # vel_x_new * air_time + x = 0
        vel_x_new = (-x) / air_time
        vel_y_new = (-y) / air_time

        # use the incoming velocities and outgoing velocities to calculate the normal for x and y direction


        # ----------------------------------------------------
        return x, y








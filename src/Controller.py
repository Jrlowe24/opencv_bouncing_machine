import tkinter as tk
import math
import time
from IK import InverseKinematics as IK
from SerialConnect import SerialConnect


class Controller(tk.Frame):

    def __init__(self, master):
        super().__init__()
        self.master = master
        self.canvas = tk.Canvas(master, bg='black')
        self.canvas.pack(side='left', fill='both', expand=True)
        self.joystick_center_x = 200
        self.joystick_center_y = 150
        r = 130
        self.canvas.create_oval((self.joystick_center_x-r), (self.joystick_center_y-r), (self.joystick_center_x+r), (self.joystick_center_y+r), outline="#f50")
        r = 20
        self.joystick = self.canvas.create_oval((self.joystick_center_x-r), (self.joystick_center_y-r), (self.joystick_center_x+r), (self.joystick_center_y+r), outline="#f50", fill="#f50")
        self.x = self.winfo_pointerx()
        self.y = self.winfo_pointery()
        self.vector = (0,0,1)
        self.slider = tk.Scale(master, from_=55, to=0, length=450, tickinterval=.25, orient='vertical')
        self.slider.pack()

        points = (0, 0, 0), (-103.9, -90, 0), (0, 90, 0), (103.9, -90, 0)
        self.IK = IK(63, 60, points, False)
        self.bounce_timer = 0
        self.bouncing = False
        self.count = 0
        self.vy = 100
        self.connection = SerialConnect('/dev/cu.wchusbserial1420', 115200)

    def update(self):
        self.x = self.winfo_pointerx()
        self.y = self.winfo_pointery() - 40
        r = 20
        dist = math.sqrt((self.x - self.joystick_center_x)**2 + (self.y - self.joystick_center_y)**2)
        if dist < 130 - r:
            self.canvas.coords(self.joystick, (self.x - r), (self.y - r), (self.x + r), (self.y + r))
            self.vector = (self.x-self.joystick_center_x, -(self.y-self.joystick_center_y), 800)
            # print(self.vector)
        else:
            self.canvas.coords(self.joystick, (self.joystick_center_x - r), (self.joystick_center_y - r), (self.joystick_center_x + r), (self.joystick_center_y + r))
            self.vector = (0,0,1)

        if self.count <= 0:
            self.vy = self.vy * -1
            # self.vector = (0, self.vy, 800)
            # self.vector = (self.vy, 0, 800)
            # print(self.vector)
            # self.IK.update(self.vector)
            # self.connection.send([self.IK.angle1, self.IK.angle2, self.IK.angle3])
            self.count = 100
        self.count -= 1

        if self.bounce_timer <= 0 and self.bouncing:
            self.IK.center_z -= 20
            self.bouncing = False

        if not self.bouncing:
            self.IK.center_z = self.slider.get()

        self.IK.update(self.vector)
        self.connection.send([self.IK.angle1, self.IK.angle2, self.IK.angle3])
        self.bounce_timer -= 1


def callback(event):
    if not controller.bouncing:
        r = 20
        dist = math.sqrt((event.x - controller.joystick_center_x) ** 2 + (event.y - controller.joystick_center_y) ** 2)
        if dist < 130 - r:
            controller.bouncing = True
            controller.bounce_timer = 10
            controller.IK.center_z += 20


if __name__ == "__main__":
    root = tk.Tk()
    root.resizable(False, False)
    root.title('Ball Bouncer IK Controller')
    root.geometry('500x300+0+0')
    root.bind("<Button-1>", callback)
    controller = Controller(root)
    while True:
        controller.update()
        root.update_idletasks()
        root.update()


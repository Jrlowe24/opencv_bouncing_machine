import cv2 as cv
import numpy as np
import math
from Bouncer import BounceController
import time
from Capture import ThreadedVideoStream, Frame
from Terminal import bcolors
import argparse
from threading import Thread


class Main:

    def __init__(self, cap, display):
        self.stopped = False
        self.tracker = BallTracker(cap, 120, True).start()
        self.frames = 0
        self.display = display
        self.update()

    def update(self):

        while True:
            if self.tracker.rendering_frames is not None:
                for frame in self.tracker.rendering_frames:
                    if self.tracker.rendering_frames[frame] is not None:
                        if frame == "pos_gui":
                            cv.imshow(frame, self.tracker.rendering_frames[frame].img)
                        elif self.frames > self.display:
                            # print('rendering frame: ', self.tracker.rendering_frames[frame].idx)
                            cv.imshow(frame, self.tracker.rendering_frames[frame].img)
                if self.frames > self.display:
                    self.frames = 0

            self.frames += 1
            if cv.waitKey(1) == ord("q"):
                self.stopped = True
                break
        self.tracker.stop()


class FindCircle:

    def __init__(self, frame, prev_points, num_samples = 20):
        self.center = 0, 0
        self.diameter = 0
        self.p1 = None
        self.p2 = None
        self.frame = frame
        self.numSamples = num_samples
        self.contours = None
        self.prev_points = prev_points
        self.mtx = np.array([[613.64160885, 0.0, 327.867], [0.0, 612.87421876, 233.204238], [0.0, 0.0, 1.0]])
        px = 0
        py = 0
        self.edges = cv.Canny(frame, 100, 200)
        indices = np.where(self.edges != [0])
        self.coordinates = list(zip(indices[1], indices[0]))
        if len(self.coordinates) > 10:
            self.p1 = self.coordinates[0]
            rand_indices = np.random.randint(1, len(indices[0]), self.numSamples)
            for i in rand_indices:
                x1, y1 = self.p1
                x2, y2 = self.coordinates[i]
                distance = math.sqrt((y1 - y2) ** 2 + (x1 - x2) ** 2)
                if distance > self.diameter:
                    self.diameter = distance
                    self.p2 = x2, y2

            self.contours, self.hierarchy = cv.findContours(self.edges.copy(), cv.RETR_TREE, cv.CHAIN_APPROX_SIMPLE)
            c = max(self.contours, key=cv.contourArea)
            self.contours = c
            # cnt = self.contours[0]
            M = cv.moments(c)

            x1 = M['m10']
            x2 = M['m00']
            if x2 != 0:
                px = int(x1 / x2)
            else:
                px = 0

            y1 = M['m01']
            y2 = M['m00']
            if y2 != 0:
                py = int(y1 / y2)
            else:
                py = 0

        z = self.calculate_z()
        x = int((px - self.mtx[0][2]) * z / self.mtx[0][0])
        y = int((py - self.mtx[1][2]) * z / self.mtx[1][1])
        self.center = px, py, x, y, z
        # self.center = self.clean_ball_data(px, py, x, y, z)

    def clean_ball_data(self, px, py, x, y, z, ref_points=30):
        points = self.prev_points[-ref_points:]
        if len(points) > ref_points - 1:
            x_data = [point[0] for point in points]
            y_data = [point[1] for point in points]
            z_data = [point[2] for point in points]

            delta_x = sum([(b - a) for a, b in zip(x_data[::1], x_data[1::1])]) / ref_points

            predict_x = x_data[-1] + delta_x
            print("last_x: ", x_data[-1])
            print("delta_x:", delta_x)
            print("predict_x: ", predict_x)
            print("actual_x: ", x)

            delta_y = sum([(b - a) for a, b in zip(y_data[::1], y_data[1::1])]) / ref_points
            predict_y = y_data[-1] + delta_y

            delta_z = sum([(b - a) for a, b in zip(y_data[::1], y_data[1::1])]) / ref_points
            # might want to use some sort of seasonal filtering for z, bounce shape is predictable
            predict_z = z_data[-1] + delta_z

            error_x = abs(x - predict_x)
            error_y = abs(y - predict_y)
            error_z = abs(z - predict_y)
            if error_x > 3:
                # print(predict_x)
                x = predict_x
            if error_y > 3:
                y = predict_y
            # if error_z > 1.5:
            #     z = predict_z
            #     self.calculate_diameter(z)
        return px, py, int(x), int(y), int(z)
        #  take the last x points and compute an average delta vector
        #  apply this to the new point and compute predicted position
        #  allow a margin of error threshold between predicted and returned position

    def calculate_z(self):
        z = int( 59.33473 + (16988590 - 59.33473)/(1 + (self.diameter/0.001584409)**1.027448))
        return z - 32.5

    def calculate_diameter(self,z):
        self.diameter = round(31.48686 + (129220700 - 31.48686)/(1 + (z/0.09187397)**1.820323))


class BallTracker:

    def __init__(self, cap, fps, visual_overlay):

        # -------------------- OpenCV ---------------------
        self.px = 0  # pixel location
        self.py = 0
        self.x = 0  # real world location
        self.y = 0
        self.z = 0
        self.path = []  # history of ball locations
        self.diameter = 0  # ball diameter
        self.visual_overlay = visual_overlay  # show ball tracking overlay
        self.positionGui = PositionGui(360, 640)
        self.controller = BounceController()
        self.frame_rate = fps  # frame rate the ball tracker loop runs on
        self.time_per_frame = 1 / self.frame_rate
        self.prev = 0  # prev time
        self.frame_speeds = []  # previous frame times for bounce controller to use
        self.fps = 0  # averaged FPS value, updates every 10 frames

        self.mtx = np.array([[613.64160885, 0.0,  327.867], [0.0, 612.87421876, 233.204238], [0.0, 0.0, 1.0]])

        print(bcolors.BOLD,'Capture Started:', bcolors.ENDC)
        print(bcolors.HEADER, 'FPS: ', bcolors.OKGREEN, self.frame_rate, bcolors.HEADER, 'Capture Resolution: ', bcolors.OKGREEN, int(cap.stream.get(3)), 'x', int(cap.stream.get(4)), bcolors.ENDC)
        print(bcolors.BOLD, "\'q\' to quit", bcolors.ENDC)

        self.rendering_frames = {'raw': None, 'output': None, 'pos_gui': None}
        cv.namedWindow('raw')
        cv.namedWindow('output')
        cv.namedWindow('pos_gui')
        self.stopped = False
        self.last_frame_idx = -1

    def start(self):
        # start the thread to read frames from the video stream
        Thread(target=self.loop, args=()).start()
        return self

    def loop(self):
        while not self.stopped:
            time_elapsed = time.time() - self.prev
            if time_elapsed >= self.time_per_frame:
                frame_cap = cap.read()
                if frame_cap.idx is not self.last_frame_idx:
                    # if the current frame is the same as the prev frame, cap() can't keep up, skip this loop
                    self.prev = time.time()
                    # frame = self.undistort(frame)
                    frame = self.rescale_frame(frame_cap.img, 50)  # 640 x 360

                    # -------------IMAGE FILTERING----------------
                    hsv = cv.cvtColor(frame, cv.COLOR_BGR2HSV)

                    lower_orange = np.array([8, 130, 175])
                    upper_orange = np.array([23, 255, 255])
                    mask = cv.inRange(hsv, lower_orange, upper_orange)

                    img2 = cv.bitwise_and(frame, frame, mask=mask)

                    img2 = cv.medianBlur(img2, 5)
                    gray = cv.cvtColor(img2, cv.COLOR_BGR2GRAY)
                    # ---------------------------------------------

                    # -----------------BALL DATA-------------------
                    circle = FindCircle(gray, self.path)  # find the circle defining the ping pong ball
                    self.diameter = circle.diameter
                    self.px = circle.center[0]  # get positional data from circle object
                    self.py = circle.center[1]

                    # self.x = int((self.px - self.mtx[0][2]) * self.z / self.mtx[0][0])  # real world x offset from center
                    # self.y = int((self.py - self.mtx[1][2]) * self.z / self.mtx[1][1])   # real world y offset from center
                    self.x = circle.center[2]
                    self.y = circle.center[3]
                    self.z = circle.center[4]
                    # self.x, self.y, self.z = 0, 0, 0
                    self.path.append((self.x, self.y, self.z))  # keep track of past 80 frame positions
                    if len(self.path) > 80:
                        self.path.pop(0)
                    self.frame_speeds.append(time_elapsed)  # keep track of past 80 frame times
                    if len(self.frame_speeds) > 80:
                        self.frame_speeds.pop(0)

                    self.controller.update(self.path, self.frame_speeds)
                    # ---------------------------------------------

                    # --------------RENDERING OUTPUT--------------
                    raw = cv.circle(frame, (int(len(frame[0]) / 2), int(len(frame) / 2)), 2, (0, 0, 255), -1)
                    img3 = cv.cvtColor(gray, cv.COLOR_GRAY2BGR)
                    self.fps = int(sum(1 / x for x in self.frame_speeds[-10:]) / 10)
                    output = self.draw_visuals(img3, circle.contours, False, self.visual_overlay)
                    gui = self.positionGui.update(self.path, self.controller.bounce_loc)
                    raw = Frame(raw, frame_cap.idx)
                    output = Frame(output, frame_cap.idx)
                    gui = Frame(gui, frame_cap.idx)
                    self.rendering_frames["raw"] = raw
                    self.rendering_frames["output"] = output
                    self.rendering_frames["pos_gui"] = gui
                    # --------------------------------------------
                # print(round(1/time_elapsed))
                self.last_frame_idx = frame_cap.idx
        cap.stop()
        cv.destroyAllWindows()

    def stop(self):
        # indicate that the thread should be stopped
        self.stopped = True

    def undistort(self, image):
        dist = np.array([-0.48763349, 0.38863045, -0.00199551, -0.00156447, -0.25809292])
        newcameramtx, _ = cv.getOptimalNewCameraMatrix(self.mtx, dist, (640, 360), 0.5)
        return cv.undistort(image, self.mtx, dist, None, newcameramtx)

    def draw_visuals(self, frame, contours, use_contours, visual_overlay):

        output = frame

        if contours is not None and visual_overlay:
            if use_contours:
                output = cv.drawContours(output, contours, -1, (0, 255, 0), -1)
            else:
                output = cv.circle(output, (self.px, self.py), 2, (0, 0, 255), -1)
                output = cv.circle(output, (self.px, self.py), int(self.diameter / 2), (0, 255, 0), 1)

        output = cv.putText(output, 'Circle Diameter: ', (20, 20), cv.FONT_HERSHEY_PLAIN,
                            1, (255, 255, 255), 2, cv.LINE_AA)
        output = cv.putText(output, 'Circle Center: ', (20, 40), cv.FONT_HERSHEY_PLAIN,
                            1, (255, 255, 255), 2, cv.LINE_AA)
        output = cv.putText(output, 'FPS: ', (20, 60), cv.FONT_HERSHEY_PLAIN,
                            1, (255, 255, 255), 2, cv.LINE_AA)
        output = cv.putText(output, str(round(self.diameter)), (165, 20), cv.FONT_HERSHEY_PLAIN,
                            1, (255, 255, 255), 2, cv.LINE_AA)
        output = cv.putText(output, str(self.x) + ', ' + str(self.y) + ', ' + str(282 - self.z), (165, 40),
                            cv.FONT_HERSHEY_PLAIN,
                            1, (255, 255, 255), 2, cv.LINE_AA)
        output = cv.putText(output, str(self.fps), (165, 60), cv.FONT_HERSHEY_PLAIN, 1, (255, 255, 255), 2, cv.LINE_AA)

        return output

    @staticmethod
    def rescale_frame(frame, percent=40):
        width = int(frame.shape[1] * percent / 100)
        height = int(frame.shape[0] * percent / 100)
        dim = (width, height)
        return cv.resize(frame, dim, interpolation=cv.INTER_AREA)


class PositionGui:

    def __init__(self, height, width):
        self.gui = np.zeros((height, width, 3), np.uint8)
        self.width = width
        self.height = height
        self.dashed_length = 10
        self.gap_length = 10
        self.pos_size = 10

    def update(self, path, bounce_loc):
        self.gui = np.zeros((self.height, self.width, 3), np.uint8)
        start_y = 0
        end_y = self.dashed_length
        while end_y < self.height:
            cv.line(self.gui, (int(self.width / 2), start_y), (int(self.width / 2), end_y), (255, 255, 255), 1)
            start_y = start_y + self.dashed_length + self.gap_length
            end_y = end_y + self.dashed_length + self.gap_length
        start_y = 0
        end_y = self.dashed_length
        while end_y < self.width:
            cv.line(self.gui, (start_y, int(self.height / 2)), (end_y, int(self.height / 2)), (255, 255, 255), 1)
            start_y = start_y + self.dashed_length + self.gap_length
            end_y = end_y + self.dashed_length + self.gap_length

        x, y, d = path[-1]
        x += int(self.width / 2)
        y += int(self.height / 2)
        cv.line(self.gui, (x - self.pos_size, y), (x + self.pos_size, y), (255, 255, 0), 1)
        cv.line(self.gui, (x, y - self.pos_size), (x, y + self.pos_size), (255, 255, 0), 1)

        x, y = bounce_loc
        x += int(self.width / 2)
        y += int(self.height / 2)
        cv.line(self.gui, (x - self.pos_size, y), (x + self.pos_size, y), (0, 0, 255), 1)
        cv.line(self.gui, (x, y - self.pos_size), (x, y + self.pos_size), (0, 0, 255), 1)

        y_pos = 15
        radius = 4
        path = path[::-1]
        for pos in path:
            x, y, d = pos
            x += int(self.width / 2)
            y += int(self.height / 2)
            cv.circle(self.gui, (x , y_pos), radius, (0, 255, 0), 1)
            cv.circle(self.gui, (y_pos, y), radius, (0, 165, 255), 1)
            y_pos = y_pos + radius * 2


        # cv.imshow('position', self.gui)
        return self.gui


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-f', '--fps', type=int, default=120, help="FPS to run openCV")
    parser.add_argument('-d', '--display', type=int, default=-1, help="Whether or not camera feed is displayed. This Value indicates the number of feed frames to skip for image render")
    parser.add_argument('-v', '--visual_overlay', type=bool, default=True, help="Visual Overlay on ball tracking")
    parser.add_argument('-c', '--camera_index', type=int, default=1, help="Camera index for openCV VideoCapture")
    args = vars(parser.parse_args())
    cap = ThreadedVideoStream(args['camera_index']).start()
    main = Main(cap, args['display'])


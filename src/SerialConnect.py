import serial


class SerialConnect:

    def __init__(self, port, baud):
        self.arduino = serial.Serial(port, baud)  # create serial object named arduino

    def send(self, pos):
        command = (str(pos[0]) + ', ' + str(pos[1]) + ', ' + str(pos[2]) + '\n')
        self.arduino.write(command.encode())  # write position to serial port

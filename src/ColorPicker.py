import cv2 as cv
import numpy as np
import sys
from Capture import ThreadedVideoStream
import argparse
from Terminal import bcolors


class ColorPicker:

    def nothing(self, x):
        pass

    def __init__(self, cap):
        cv.namedWindow('image')
        cv.createTrackbar('HMin', 'image', 0, 179, self.nothing)
        cv.createTrackbar('SMin', 'image', 0, 255, self.nothing)
        cv.createTrackbar('VMin', 'image', 0, 255, self.nothing)
        cv.createTrackbar('HMax', 'image', 0, 179, self.nothing)
        cv.createTrackbar('SMax', 'image', 0, 255, self.nothing)
        cv.createTrackbar('VMax', 'image', 0, 255, self.nothing)

        # Set default value for Max HSV trackbars
        cv.setTrackbarPos('HMax', 'image', 179)
        cv.setTrackbarPos('SMax', 'image', 255)
        cv.setTrackbarPos('VMax', 'image', 255)

        self.hMin = self.sMin = self.vMin = self.hMax = self.sMax = self.vMax = 0
        self.phMin = self.psMin = self.pvMin = self.phMax = self.psMax = self.pvMax = 0

        while 1:
            frame = cap.read()
            frame = frame.img
            if frame is None:
                break
            frame = cv.resize(frame, (640, 360), cv.INTER_AREA)
            while 1:
                self.hMin = cv.getTrackbarPos('HMin', 'image')
                self.sMin = cv.getTrackbarPos('SMin', 'image')
                self.vMin = cv.getTrackbarPos('VMin', 'image')
                self.hMax = cv.getTrackbarPos('HMax', 'image')
                self.sMax = cv.getTrackbarPos('SMax', 'image')
                self.vMax = cv.getTrackbarPos('VMax', 'image')

                # Set minimum and maximum HSV values to display
                lower = np.array([self.hMin, self.sMin, self.vMin])
                upper = np.array([self.hMax, self.sMax, self.vMax])

                # Convert to HSV format and color threshold
                hsv = cv.cvtColor(frame, cv.COLOR_BGR2HSV)
                mask = cv.inRange(hsv, lower, upper)
                result = cv.bitwise_and(frame, frame, mask=mask)
                result = cv.medianBlur(result, 5)

                cv.imshow('image', result)

                k = cv.waitKey(30)
                if k == ord('s'):
                    break
                elif k == ord('p'):
                    print('--------------------')
                    print(bcolors.HEADER, 'hMin: ', bcolors.OKGREEN, self.hMin)
                    print(bcolors.HEADER, 'hMax: ', bcolors.OKGREEN, self.hMax)
                    print(bcolors.HEADER, 'sMin: ', bcolors.OKGREEN, self.sMin)
                    print(bcolors.HEADER, 'vMin: ', bcolors.OKGREEN, self.vMin, bcolors.ENDC)
                elif k == ord('q'):
                    cap.stop()
                    cv.destroyAllWindows()
                    sys.exit()

        cap.stop()
        cv.destroyAllWindows()


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-c', '--camera_index', type=int, default=1, help="Camera index for openCV VideoCapture")
    args = vars(parser.parse_args())
    cap = ThreadedVideoStream(args['camera_index']).start()
    cap.stream.set(3, 1280)
    cap.stream.set(4, 720)
    ColorPicker(cap)

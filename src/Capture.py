# import the necessary packages
import time
import sys
from Terminal import bcolors
from threading import Thread
import cv2


class Frame:

    def __init__(self, img, idx):
        self.img = img
        self.idx = idx


class ThreadedVideoStream:

    def __init__(self, src):
        self.stream = cv2.VideoCapture(src)

        self.stream.set(3, 1280)
        self.stream.set(4, 720)
        self.stream.set(5, 120)
        self.idx = 0
        (self.grabbed, self.frame) = self.stream.read()
        self.curr_frame = None
        time.sleep(.2)
        if not self.grabbed:
            print(bcolors.FAIL, 'OpenCV: stream.read() did not return any frames', bcolors.ENDC)
            sys.exit()

        self.stopped = False

    def start(self):
        Thread(target=self.update, args=()).start()
        return self

    def update(self):
        while 1:
            if self.stopped:
                return

            (self.grabbed, self.frame) = self.stream.read()
            self.curr_frame = Frame(self.frame, self.idx)
            self.idx += 1

            if not self.grabbed:
                self.stopped = True

    def read(self):
        return self.curr_frame

    def stop(self):
        self.stopped = True
        print(bcolors.BOLD, 'Ending Capture', bcolors.ENDC)
